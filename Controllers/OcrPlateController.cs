﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebOCR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OcrPlateController : ControllerBase
    {
        private AzureCV<PlatePrediction> azureCV;
        public OcrPlateController(IPrediction<PlatePrediction> platePrediction, OCR oCR)
        {
            azureCV = new AzureCV<PlatePrediction>(platePrediction, oCR);
        }


        [HttpPost("image")]
        public async Task<IActionResult> Image() 
        {
            try
            {
                using (var ms = new MemoryStream())
                {
                    await Request.Body.CopyToAsync(ms);
                    return Ok(await azureCV.ExtractTextFromImage(ms.ToArray()));
                }
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
