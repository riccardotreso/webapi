﻿using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Prediction.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace WebOCR
{
    public class AzureCV<T>
    {
        private IPrediction<T> prediction;
        private OCR oCR;

        public AzureCV(IPrediction<T> prediction, OCR oCR)
        {
            this.prediction = prediction;
            this.oCR = oCR;
        }
        public async Task<IList<ComputerVisionResult>> ExtractTextFromImage(byte[] imageData)
        {

            IList<ComputerVisionResult> computerVisionResults = new List<ComputerVisionResult>();
            IAsyncEnumerator<PredictionResult> predictions = prediction.GetPredictionResultFromImage(imageData).GetAsyncEnumerator();
            string ocrText;
            try
            {
                while (await predictions.MoveNextAsync())
                {
                    ocrText = await oCR.ReadTextFromImageData(predictions.Current.Image);
                    computerVisionResults.Add(new ComputerVisionResult
                    {
                        Prediction = predictions.Current,
                        Text = ocrText
                    });
                }

            }
            finally { if (predictions != null) await predictions.DisposeAsync(); }

            return computerVisionResults;
        }
    }

    public class PredictionResult
    {
        public byte[] Image { get; set; }
        public double Probability { get; set; }
    }

    public class ComputerVisionResult
    {
        public PredictionResult Prediction { get; set; }
        public string Text { get; set; }
    }


    public interface IPrediction<T>
    {
        IAsyncEnumerable<PredictionResult> GetPredictionResultFromImage(byte[] sourceImage);
    }

    public class PlatePrediction : IPrediction<PlatePrediction>
    {
        string url;
        HttpClient client;

        public PlatePrediction(string key, string url)
        {
            this.client = new HttpClient();
            this.client.DefaultRequestHeaders.Add("Prediction-Key", key);
            this.url = url;
        }

        public async IAsyncEnumerable<PredictionResult> GetPredictionResultFromImage(byte[] sourceImage)
        {

            HttpResponseMessage response;
            using (var content = new ByteArrayContent(sourceImage))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                response = await client.PostAsync(url, content);
                var result = JsonConvert.DeserializeObject<ImagePrediction>(await response.Content.ReadAsStringAsync());
                var maxProbability = result.Predictions.Where(y => y.Probability > 0.4);
                using (MemoryStream ms = new MemoryStream(sourceImage))
                {
                    Bitmap img = (Bitmap)Image.FromStream(ms);

                    foreach (var prob in maxProbability)
                    {
                        Rectangle cropRect = new Rectangle((int)(prob.BoundingBox.Left * img.Width), (int)(prob.BoundingBox.Top * img.Height), (int)(prob.BoundingBox.Width * img.Width), (int)(prob.BoundingBox.Height * img.Height));
                        if (cropRect.Width > 50 && cropRect.Height > 50)
                        {
                            Bitmap target = new Bitmap(cropRect.Width, cropRect.Height);
                            using (Graphics g = Graphics.FromImage(target))
                            {
                                g.DrawImage(img, new Rectangle(0, 0, target.Width, target.Height), cropRect, GraphicsUnit.Pixel);
                            }
                            using (MemoryStream msPortion = new MemoryStream())
                            {
                                target.Save(msPortion, System.Drawing.Imaging.ImageFormat.Png);
                                byte[] imagePortion = new byte[msPortion.Length];
                                imagePortion = msPortion.ToArray();
                                yield return new PredictionResult { Image = imagePortion, Probability = prob.Probability };
                            }
                        }
                    }
                }
            }
        }
    }

    public class OCR
    {
        string uriBase;
        HttpClient client;

        public OCR(string subscriptionKey, string endpoint)
        {
            uriBase = endpoint + "/vision/v3.0/read/analyze";

            client = new HttpClient();
            client.DefaultRequestHeaders.Add(
                    "Ocp-Apim-Subscription-Key", subscriptionKey);
        }

        public async Task<string> ReadTextFromImageData(byte[] byteData)
        {
            try
            {
                string url = uriBase;

                HttpResponseMessage response;
                string operationLocation = null;

                using (ByteArrayContent content = new ByteArrayContent(byteData))
                {

                    content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/octet-stream");
                    response = await client.PostAsync(url, content);
                }


                if (response.IsSuccessStatusCode)
                    operationLocation =
                        response.Headers.GetValues("Operation-Location").FirstOrDefault();
                else
                {
                    string errorString = await response.Content.ReadAsStringAsync();
                    return JToken.Parse(errorString).ToString();
                }

                string contentString;
                int i = 0;
                do
                {
                    if (i > 0)
                        System.Threading.Thread.Sleep(1000);
                    response = await client.GetAsync(operationLocation);
                    contentString = await response.Content.ReadAsStringAsync();
                    ++i;
                }
                while (i < 60 && contentString.IndexOf("\"status\":\"succeeded\"") == -1);

                if (i == 60 && contentString.IndexOf("\"status\":\"succeeded\"") == -1)
                    return "Timeout error.";

                var resobj = JsonConvert.DeserializeObject<ReadOperationResult>(JToken.Parse(contentString).ToString());
                string result = "";
                var textUrlFileResults = resobj.AnalyzeResult.ReadResults;
                foreach (ReadResult page in textUrlFileResults)
                {
                    result += string.Join(Environment.NewLine, page.Lines.Select(x => x.Text));
                }
                return result;

            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
